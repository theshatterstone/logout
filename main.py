#!/usr/bin/env python3

import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk
import os
from distro import id

homedir = os.path.expanduser('~')
username = os.environ["USER"]

distr = id()

shutdown = "systemctl poweroff"
restart = "systemctl reboot"
suspend = "systemctl suspend"
hibernate = "systemctl hibernate"

if distr == "artix":
    if os.path.isfile("/usr/bin/loginctl"):
        shutdown = "loginctl poweroff"
        restart = "loginctl reboot"
        suspend = "loginctl suspend"
        hibernate = "loginctl hibernate"

print (username)
class MainWin(Gtk.Window):
    def __init__(self):
        super().__init__(title="Shatterstone Logout")

        self.set_border_width(0)
        #self.set_default_size(1920, 1080)
        self.fullscreen()
        self.set_resizable(False)

        frame1 = Gtk.Frame(label="Shatterstone Logout")

if __name__ == "__main__":
    window = MainWin()
    window.connect("destroy", Gtk.main_quit)
    window.show_all()
    Gtk.main()


# winmain = MyWindow1()


# winmain.connect("destroy", Gtk.main_quit)


# winmain.show_all()
# Gtk.main()